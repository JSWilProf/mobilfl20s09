@echo off
set DIR=%CD%
%USERPROFILE%\Downloads\Git-2.26.2-64-bit.exe
msiexec /i %USERPROFILE%\Downloads\OpenJDK8U-jdk_x64_windows_hotspot_8u265b01.msi
set JAVA_HOME=C:\Program Files\AdoptOpenJDK\jdk-8.0.265.01-hotspot
set GIT_PATH=%ProgramFiles%\Git\cmd
cd %USERPROFILE%\Documents
"%GIT_PATH%\git" clone https://github.com/flutter/flutter.git -b stable
powershell -NoP -NonI -Command "Expand-Archive '%USERPROFILE%\Downloads\commandlinetools-win-6609375_latest.zip' '.'"
mkdir Android
mkdir Android\sdk
mkdir Android\sdk\cmdline-tools
move tools Android\sdk\cmdline-tools\latest
setx ANDROID_SDK_ROOT %USERPROFILE%\Documents\Android\sdk
set ANDROID_SDK_ROOT=%USERPROFILE%\Documents\Android\sdk
setx PUB_HOSTED_URL https://pub.dev/
set PUB_HOSTED_URL=https://pub.dev/
setx PATH "%PATH%;%USERPROFILE%\Documents\flutter\bin;%USERPROFILE%\Documents\flutter\bin\cache\dart-sdk\bin"
set PATH=%JAVA_HOME%\bin;%PATH%;%USERPROFILE%\Documents\flutter\bin;%USERPROFILE%\Documents\flutter\bin\cache\dart-sdk\bin;%GIT_PATH%
set TOOLS=%ANDROID_SDK_ROOT%\cmdline-tools\latest\bin
set CMD=%TOOLS%\sdkmanager.bat
call %CMD% --install build-tools;30.0.2
call %CMD% --install emulator
call %CMD% --install extras;google;google_play_services
call %CMD% --install extras;google;market_apk_expansion
call %CMD% --install extras;google;market_licensing
call %CMD% --install extras;intel;Hardware_Accelerated_Execution_Manager
call %CMD% --install extras;google;usb_driver
call %CMD% --install patcher;v4
call %CMD% --install platform-tools
call %CMD% --install platforms;android-29
call %CMD% --install system-images;android-29;google_apis_playstore;x86_64
call %CMD% --licenses
call %TOOLS%\avdmanager.bat create avd -d 18 -k system-images;android-29;google_apis_playstore;x86_64 -n Pixel_API_29 --force
cd %ANDROID_SDK_ROOT%\extras\intel\Hardware_Accelerated_Execution_Manager\
call silent_install.bat
cd %DIR%
cmd /C flutter doctor -v
pause