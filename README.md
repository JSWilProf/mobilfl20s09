# Repositório da Turma Desenvolvimento Mobile com Flutter - Fevereiro/Março de 2020
## Bem Vindo
Aqui são disponibilizados os projetos Flutter, as apresentações, exercícios e respostas.
## Como obter
Para obter uma cópia deste conteúdo basta utilizar o comando:

```
git clone https://gitlab.com/JSWilProf/mobilfl20s09.git
```

Também é possível fazer o download através do link
[Flutter](https://gitlab.com/JSWilProf/mobilfl20s09)

# Ementa

## Programação do Curso (60h)

- Apresentação da Linguagem de Programação
- Tipos de dados da Linguagem
- Variáveis e constantes
- Conversão entre tipos
- Operadores
- Estruturas de decisão
- Estruturas de Repetição
- Funções
- Orientação à Objetos
- Introdução ao desenvolvimento Mobile
- Framework Flutter
- Recursos avançados
- Armazenamento das Informações


