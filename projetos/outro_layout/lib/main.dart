import 'package:flutter/material.dart';

void main() => runApp(ReceitaApp());

class ReceitaApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.orange,
      ),
      home: ReceitaPage(title: 'Flutter Demo Home Page'),
    );
  }
}

class ReceitaPage extends StatefulWidget {
  ReceitaPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _ReceitaPageState createState() => _ReceitaPageState();
}

class _ReceitaPageState extends State<ReceitaPage> {
  @override
  Widget build(BuildContext context) {
    var description = Container(
      padding: EdgeInsets.fromLTRB(20, 10, 20, 20),
      child: Text(
        '    Pavlova é uma sobremesa à base '
        'de merengue com o nome de bailarina russa '
        'Anna Pavlova.\n    Pavlova apresenta uma '
        'crosta crocante e macio, leve por '
        'dentro, coberto com frutas e chantilly.',
        textAlign: TextAlign.justify,
        overflow: TextOverflow.visible,
        maxLines: 10,
        style: TextStyle(
          fontFamily: 'Roboto',
          fontWeight: FontWeight.w200,
          letterSpacing: 0.5,
          fontSize: 20,
        ),
      ),
    );

    var title = Container(
        padding: EdgeInsets.fromLTRB(20, 20, 20, 10),
        child: Text(
          'Pavlora de Morango',
          textAlign: TextAlign.justify,
          overflow: TextOverflow.visible,
          maxLines: 10,
          style: TextStyle(
            fontFamily: 'Roboto',
            fontWeight: FontWeight.w900,
            letterSpacing: 0.5,
            fontSize: 30,
          ),
        ));

    var style = TextStyle(
      fontFamily: 'Roboto',
      fontWeight: FontWeight.w800,
      letterSpacing: 0.5,
      fontSize: 20,
      color: Colors.black,
    );

    var icon = DefaultTextStyle.merge(
        style: style,
        child: Container(
          padding: EdgeInsets.all(20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Flexible(
                flex: 1,
                child: Column(
                  children: <Widget>[
                    Icon(Icons.kitchen, color: Colors.green[500]),
                    Text('Preparação', textScaleFactor: 0.8),
                    Text('25 min.')
                  ],
                ),
              ),
              Flexible(
                flex: 1,
                child: Column(
                  children: <Widget>[
                    Icon(Icons.timer, color: Colors.green[500]),
                    Text('Preparação', textScaleFactor: 0.8),
                    Text('25 min.')
                  ],
                ),
              ),
              Flexible(
                flex: 1,
                child: Column(
                  children: <Widget>[
                    Icon(Icons.restaurant, color: Colors.green[500]),
                    Text('Preparação', textScaleFactor: 0.8),
                    Text('25 min.')
                  ],
                ),
              )
            ],
          ),
        ));

    return SafeArea(
      top: true,
      bottom: false,
      child: Scaffold(
        body: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Image.asset('images/img0.png', fit: BoxFit.fitWidth),
              title,
              description,
              icon
            ],
          ),
        ),
      ),
    );
  }
}
