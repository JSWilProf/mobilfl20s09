import 'package:flutter/material.dart';

void main() => runApp(MaterialApp(
      theme: ThemeData(primarySwatch: Colors.green),
      home: MeuApp(titulo: 'Exemplo de Lista'),
    ));

class MeuApp extends StatefulWidget {
  final String titulo;

  MeuApp({Key key, @required this.titulo}) : super(key: key);

  @override
  _MeuAppState createState() => _MeuAppState();
}

class _MeuAppState extends State<MeuApp> {
  Widget quadroDeImagem(String imagem) {
    return Expanded(
      child: Container(
        margin: EdgeInsets.all(4),
        decoration: BoxDecoration(
            color: Colors.black12,
            border: Border.all(width: 10, color: Colors.black38),
            borderRadius: BorderRadius.all(Radius.circular(8))),
        child: Image.asset(imagem),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      bottom: false,
      child: Scaffold(
        appBar: AppBar(
          title: Text(widget.titulo),
        ),
        body: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                quadroDeImagem('imagens/img1.png'),
                quadroDeImagem('imagens/img2.png')
              ],
            ),
            Row(
              children: <Widget>[
                quadroDeImagem('imagens/img3.png'),
                quadroDeImagem('imagens/img4.png')
              ],
            ),
          ],
        ),
      ),
    );
  }
}
