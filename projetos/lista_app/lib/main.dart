import 'package:flutter/material.dart';

void main() => runApp(MaterialApp(
      theme: ThemeData(primarySwatch: Colors.blueGrey),
      home: ListaApp(),
    ));

class ListaApp extends StatefulWidget {
  @override
  _ListaAppState createState() => _ListaAppState();
}

class _ListaAppState extends State<ListaApp> {
  static Map<String, List<String>> imgDesc = {
    'img1': ['Empire Skyline', 'New York'],
    'img2': ['Sentinel Beach', 'Yosemite National Park'],
    'img3': ['Pololu Valley', 'Hawai'],
    'img4': ['Ohope Beach', 'New Zeland'],
    'img5': ['Alabama Hills', 'Day'],
    'img6': ['Alabama Hills', 'Night'],
    'img7': ['Alabama Hills', 'Sunrise'],
    'img8': ['Alabama Hills', 'Sunset'],
    'img9': ['Yosemite', 'Tioga Road'],
    'img10': ['Mojave Presenve', 'Day'],
    'img11': ['Cape Lookout', 'Sunset'],
    'img12': ['Joshua Tree', 'Sunrise'],
    'img13': ['San Francisco', 'North'],
    'img14': ['Yosemite', 'Sunset'],
    'img15': ['Pyramid Lake', 'Day'],
    'img16': ['Natural Park', 'Oregon'],
    'img17': ['Red Bridge', 'Japan'],
    'img18': ['Spring flowers', 'Asian garden'],
    'img19': ['Snowing', 'New Zeland'],
    'img20': ['Chinese Ganden', 'China'],
    'img21': ['Olive Trees', 'unknown']
  };

  var lista = imgDesc.entries.toList();

  @override
  Widget build(BuildContext context) {
    final orientacao = MediaQuery.of(context).orientation;

    return Scaffold(
      appBar: AppBar(
        title: Text('Lista de Imagens'),
      ),
      body: GridView.count(
        crossAxisCount: orientacao == Orientation.portrait ? 2 : 3,
        childAspectRatio: orientacao == Orientation.portrait ? 1.0 : 1.3,
        mainAxisSpacing: 4,
        crossAxisSpacing: 4,
        children: List.generate(
            imgDesc.length,
            (i) => GridTile(
                  child: Image.asset(
                    'imagens/${lista[i].key}.png',
                    fit: BoxFit.cover,
                  ),
                  footer: GridTileBar(
                    backgroundColor: Colors.black45,
                    title: FittedBox(
                      fit: BoxFit.scaleDown,
                      alignment: Alignment.centerLeft,
                      child: Text('${lista[i].value[0]}'),
                    ),
                    subtitle: FittedBox(
                      fit: BoxFit.scaleDown,
                      alignment: Alignment.centerLeft,
                      child: Text('${lista[i].value[1]}'),
                    ),
                    trailing: Icon(
                      Icons.star_border,
                      color: Colors.white,
                    ),
                  ),
                )),
      ),
    );
  }

  Widget controiLista(BuildContext context) {
    return ListView.builder(
        padding: EdgeInsets.all(8),
        itemCount: imgDesc.length,
        itemBuilder: constroiItem);
  }

  Widget primeiraLista(int indice) {
    return Row(
      children: <Widget>[
        SizedBox(
          width: 60,
          height: 60,
          child: Image.asset('imagens/${lista[indice].key}.png'),
        ),
        SizedBox(width: 8),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              '${lista[indice].value[0]}',
              style: TextStyle(fontWeight: FontWeight.w400, fontSize: 20),
            ),
            Text(
              '${lista[indice].value[1]}',
              style: TextStyle(fontWeight: FontWeight.w100, fontSize: 18),
            )
          ],
        )
      ],
    );
  }

  Widget constroiItem(BuildContext context, int indice) {
    return Column(
      children: <Widget>[
        ListTile(
          leading: SizedBox(
            width: 60,
            height: 60,
            child: Image.asset(
              'imagens/${lista[indice].key}.png',
              fit: BoxFit.cover,
            ),
          ),
          title: Text('${lista[indice].value[0]}'),
          subtitle: Text('${lista[indice].value[1]}'),
        ),
        Divider(height: 0, color: Colors.black12, thickness: 1)
      ],
    );
  }
}
