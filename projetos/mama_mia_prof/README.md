# App Mama Mia

## Antes de iniciar

Após clonar o projeto do GIT será necessário executar a sequencia a seguir de comandos no diretório da aplicação:

```shell
flutter clean
flutter packages get
```

Estes comandos limpam as área de compilação e baixam todas as dependências do projeto.

## Geradores de Ícones

Sites para gerar icones para o aplicativo nas plataformas IOS e Android

[App Icon Generator](https://appicon.co)
e
[Android Asset Studio](https://romannurik.github.io/AndroidAssetStudio/index.html)

## Construção do aplicativo Android APK

A publicação da aplicação na Play Store requer que seja criado um keystore. Isto pode ser feito a partir da abertura do projeto Android pela opção do menu "Open Android module in Android Studio"

O arquivo de keystore deve ser salvo na pasta "app" da aplicação android e em seguida o arquivo "build.gradle" do modulo app deve ser alterado para conter as chaves de autenticação do keystore e  o parametro "signingConfigs" deve ter a palavra "debug" alterada para "release"

```groovy
android {
 ...
    signingConfigs {
        release {
            storeFile file("o nome do arquivo keystore.jsk")
            storePassword "A senha usada pra o arquivo"
            keyAlias "o nome usado como chave"
            keyPassword "a senha da chave"
        }
    }
 ...
    buildTypes {
        release {
            signingConfig signingConfigs.release
        }
    }
}
```

Para a execução do aplicativo num emulador Android ou em um dispositivo Android conectado via USB é utilizado o comando a seguir:

```shell
flutter run
```

A preparação de um APK de avaliação é através do comando a seguir:

```shell
flutter build appbundle --build-name=1.0.0.X --release
```

Onde aparece 1.0.0.X temos o nª da versão do Build encontrado no arquivo pubspec.yaml.

## Construção do aplicativo para IOS

A construção do aplicativo para IOS segue o mesmo procedimento que para Android, vide a seguir:

```shell
flutter build ios --build-name=1.0.0.X --profile
```

Esta execução deve ocorrer em um equipamento com Sistema Operacional **macOS** e com o **XCode** instalado e com as contas da Inscrição no programa de desenvolvedores Apple configurada.

Antes da do processo deve ser aberto o projeto com o **XCode**, que pode ser iniciado a partir do **Android Studio** no mesmo ambiente operacional, ou com o comando:

```shell
open ios/Runner.xcworkspace
```

No **XCode** deve ser identificado o **Provisioning Profile** assinado por um certificado e o **Team ID**. Após este passo pode ser utilzado o próprio **XCode** para a publicação na loja da Apple.
