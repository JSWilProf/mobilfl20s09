import 'dart:async';
import 'dart:typed_data';

import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mama_mia/bloc/auth_bloc.dart';
import 'package:mama_mia/bloc/foto_bloc.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

class AppDrawer extends StatefulWidget {
  final PanelController controller;

  AppDrawer({Key key, this.controller}) : super(key: key);

  @override
  _AppDrawerState createState() => _AppDrawerState();
}

class _AppDrawerState extends State<AppDrawer> {
  final _authBloc = BlocProvider.getBloc<AuthBloc>();
  final _fotoBloc = BlocProvider.getBloc<FotoBloc>();
  final _fotoKey = GlobalKey();
  StreamSubscription<Uint8List> _fotoSub;
  StreamSubscription<User> _userSub;
  ImageProvider _fotoProvider;
  String _titulo = '';
  bool _estaLogado = false;

  @override
  void initState() {
    super.initState();
    FocusManager.instance.primaryFocus.unfocus();
    _setupFotoeTitulo();
  }

  _setupFotoeTitulo() {
    _fotoSub = _fotoBloc.localFoto.listen((foto) {
      setState(() {
        _fotoProvider =
            foto != null ? MemoryImage(foto) : AssetImage('imagens/logo.jpg');
      });
    });

    _userSub = _authBloc.state.listen((user) {
      setState(() {
        _estaLogado = user != null;
        _titulo = user != null && user.email != null ? user.email : 'Mama Mia';
      });
    });
  }

  @override
  void dispose() {
    _fotoSub.cancel();
    _userSub.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          DrawerHeader(
            decoration: BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.fitWidth,
                    image: AssetImage('imagens/drawer.jpg'))),
            child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CircleAvatar(
                    backgroundColor: Colors.white,
                    radius: 42,
                    child: GestureDetector(
                      child: CircleAvatar(
                          key: _fotoKey,
                          radius: 40,
                          backgroundColor: Colors.white,
                          backgroundImage: _fotoProvider),
                      onTap: () {
                        if (_estaLogado) {
                          _showMenu().then((opcao) {
                            switch (opcao) {
                              case 1:
                                _pickImage(ImageSource.camera);
                                break;
                              case 2:
                                _pickImage(ImageSource.gallery);
                                break;
                              case 3:
                                _removeFoto();
                            }
                          });
                        }
                      },
                    ),
                  ),
                  SizedBox(height: 25),
                  Text(
                    _titulo,
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w600,
                        fontSize: 20,
                        shadows: [
                          BoxShadow(
                              color: Colors.grey[900], offset: Offset(2, 2))
                        ]),
                  )
                ],
              )
            ]),
          ),
          ListTile(
            title: Text('Login'),
            leading: Icon(FontAwesomeIcons.signInAlt),
            enabled: !_estaLogado,
            onTap: () async {
              widget.controller.isPanelClosed
                  ? widget.controller.close()
                  : widget.controller.show();
              widget.controller.animatePanelToPosition(1);
              Navigator.of(context).pop();
            },
          ),
          ListTile(
            title: Text('Logout'),
            leading: Icon(FontAwesomeIcons.signOutAlt),
            enabled: _estaLogado,
            onTap: () async {
              _authBloc.logoff();
              Navigator.of(context).pop();
            },
          )
        ],
      ),
    );
  }

  _removeFoto() {
    _fotoBloc.deleteLocalFoto();
  }

  Future<int> _showMenu() async {
    final RenderBox rb = _fotoKey.currentContext.findRenderObject();
    final local = rb.localToGlobal(Offset.zero);
    double esquerda = local.dx + 50;
    double acima = local.dy + 50;
    return await showMenu(
        context: context,
        position:
            RelativeRect.fromLTRB(esquerda, acima, esquerda + 10, acima + 10),
        items: [
          PopupMenuItem(child: Text('Tirar Foto'), value: 1),
          PopupMenuItem(child: Text('Galeria de Foto'), value: 2),
          PopupMenuItem(child: Text('Excluir Foto'), value: 3)
        ]);
  }

  _pickImage(ImageSource source) {
    ImagePicker().getImage(source: source).then((file) {
      if (file == null) return;
      ImageCropper.cropImage(
              sourcePath: file.path,
              maxWidth: 80,
              maxHeight: 80,
              androidUiSettings:
                  AndroidUiSettings(toolbarTitle: 'Edita a Foto'),
              iosUiSettings: IOSUiSettings(title: 'Edita a Foto'))
          .then((newFile) async {
        final bytes = await newFile.readAsBytes();
        _fotoBloc.setLocalFoto(bytes);
      });
    });
  }
}
