import 'package:flutter/material.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

Widget configuraOSlidingPainel(BuildContext context,
      PanelController controller, Widget paginaPrincipal,
      Widget child
    ) {
  final orientacao = MediaQuery.of(context).orientation;

  return SlidingUpPanel(
    margin: orientacao == Orientation.portrait
      ? EdgeInsets.only(left: 5, right: 5)
      : EdgeInsets.only(left: 50, right: 300, top: 100),
    padding: EdgeInsets.only(top: 5, left: 8, right: 8),
    controller: controller,
    borderRadius: BorderRadius.only(
      topRight: Radius.circular(24),
      topLeft: Radius.circular(24)
    ),
    maxHeight: 400,
    minHeight: 0,
    panel: Center(
      child: Container(
        width: MediaQuery.of(context).size.width -
            (orientacao == Orientation.portrait ? 20 : 100),
        height: MediaQuery.of(context).size.height,
        child: child,
      ),
    ),
    body: paginaPrincipal,
  );
}