import 'package:flutter/material.dart';

void alerta(
    {@required BuildContext context,
    @required GlobalKey<ScaffoldState> scaffoldKey,
    @required String mensagem,
    bool erro = false}) {
  scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(mensagem),
      backgroundColor: erro ? Colors.red[600] : Colors.green[600],
      duration: Duration(seconds: 3)));
}
