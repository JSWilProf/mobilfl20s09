import 'package:flutter/material.dart';

class Dimensoes {
  final BuildContext context;

  Dimensoes({@required this.context});

  Orientation get _orientation => MediaQuery.of(context).orientation;
  bool get isPortrait => _orientation == Orientation.portrait;
  double get altura => MediaQuery.of(context).size.height;
  double get largura => MediaQuery.of(context).size.width;
  bool get isPequeno => largura <= 320;
  double get lado => isPortrait
      ? largura > 500 ? 155.0 : largura / 2 - 36
      : largura > 800 ? 184.0 : largura / 3 - 54;
  int get qtdGrades =>
      isPortrait ? largura > 500 ? 4 : 2 : largura > 800 ? 5 : 3;
  double get aspectRatio => lado / (lado + (isPortrait ? 70 : 120));
  double get expandedHeight => isPortrait ? altura / 5 : altura / 4;
}
