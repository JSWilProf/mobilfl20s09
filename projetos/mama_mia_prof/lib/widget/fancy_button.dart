import 'package:flutter/material.dart';

class FancyButton extends StatelessWidget {
  final IconData icone;
  final GestureTapCallback onTap;
  final Color color;

  FancyButton(this.icone, {@required this.onTap, this.color = Colors.green});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
        width: 25,
        height: 25,
        decoration: BoxDecoration(color: color, shape: BoxShape.circle),
        child: Icon(icone),
      ),
      onTap: onTap,
    );
  }
}
