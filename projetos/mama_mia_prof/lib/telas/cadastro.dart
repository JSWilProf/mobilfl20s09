import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:mama_mia/bloc/auth_bloc.dart';
import 'package:mama_mia/dados/cliente.dart';
import 'package:mama_mia/widget/dimensoes.dart';
import 'package:mama_mia/widget/funcoes.dart';
import 'package:progress_dialog/progress_dialog.dart';

class Cadastro extends StatefulWidget {
  @override
  _CadastroState createState() => _CadastroState();
}

class _CadastroState extends State<Cadastro> {
  final _nomeController = TextEditingController();
  final _emailController = TextEditingController();
  final _senhaController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final _authBloc = BlocProvider.getBloc<AuthBloc>();

  @override
  Widget build(BuildContext context) {
    final dim = Dimensoes(context: context);
    final progressDialog = ProgressDialog(context)
      ..style(message: 'Criando a Conta...');

    void criaConta() {
      if (_formKey.currentState.validate()) {
        var cliente = Cliente()
          ..nome = _nomeController.text
          ..email = _emailController.text;

        progressDialog.show();

        _authBloc.inscrever(
            cliente: cliente,
            senha: _senhaController.text,
            noOk: () => progressDialog
                .hide()
                .then((value) => Navigator.of(context).pop()),
            noErro: () => progressDialog.hide().then((value) => alerta(
                context: context,
                scaffoldKey: _scaffoldKey,
                mensagem: 'Houve falha ao criar a Conta',
                erro: true)));
      }
    }

    Widget criaCampo(String texto, TextEditingController ctrl,
        FormFieldValidator<String> validador,
        {bool senha = false}) {
      return TextFormField(
          controller: ctrl,
          decoration: InputDecoration(hintText: texto),
          validator: validador,
          obscureText: senha);
    }

    Widget configuraPagina(BuildContext context) {
      return Form(
        key: _formKey,
        child: ListView(
          padding: EdgeInsets.all(16),
          children: [
            criaCampo('Nome Completo', _nomeController,
                (value) => value.isEmpty ? "Nome Inválido" : null),
            SizedBox(height: 16),
            criaCampo(
                'E-Mail',
                _emailController,
                (value) => value.isEmpty || !value.contains('@')
                    ? "E-Mail Inválido"
                    : null),
            SizedBox(height: 16),
            criaCampo(
                'Senha',
                _senhaController,
                (value) =>
                    value.isEmpty || value.length < 6 ? "Senha Inválida" : null,
                senha: true),
            SizedBox(height: 16),
            RaisedButton(
                child: Text('Criar'),
                textColor: Colors.white,
                color: Colors.green,
                onPressed: criaConta),
            SizedBox(height: 16),
            RaisedButton(
                child: Text('Cancelar'),
                textColor: Colors.white,
                color: Colors.red,
                onPressed: () => Navigator.of(context).pop())
          ],
        ),
      );
    }

    Widget configuraCabecalho(BuildContext context) {
      return SliverAppBar(
        elevation: 2,
        expandedHeight: dim.isPortrait ? 200 : 100,
        floating: true,
        pinned: true,
        snap: true,
        backgroundColor: Colors.white,
        flexibleSpace: Container(
          decoration: BoxDecoration(color: Colors.white),
          child: Stack(fit: StackFit.expand, children: [
            Image.asset('imagens/inscricao.png', fit: BoxFit.cover),
            FlexibleSpaceBar(
              centerTitle: true,
              title: Text('Criar conta',
                  style: TextStyle(color: Colors.white, fontSize: 24, shadows: [
                    Shadow(
                        blurRadius: 0.4,
                        offset: Offset(0.5, 0.5),
                        color: Colors.black)
                  ])),
            )
          ]),
        ),
      );
    }

    return Scaffold(
      key: _scaffoldKey,
      body: NestedScrollView(
        headerSliverBuilder: (context, _) => [configuraCabecalho(context)],
        body: SafeArea(
          top: dim.isPortrait ? false : true,
          bottom: false,
          child: configuraPagina(context),
        ),
      ),
    );
  }
}
