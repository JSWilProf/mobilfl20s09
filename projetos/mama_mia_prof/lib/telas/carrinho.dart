import 'dart:collection';

import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mama_mia/bloc/carrinho_bloc.dart';
import 'package:mama_mia/bloc/pizza_bloc.dart';
import 'package:mama_mia/dados/item_carrinho.dart';
import 'package:mama_mia/widget/dimensoes.dart';
import 'package:mama_mia/widget/fancy_button.dart';

class Carrinho extends StatefulWidget {
  @override
  _CarrinhoState createState() => _CarrinhoState();
}

class _CarrinhoState extends State<Carrinho> {
  final _fmt = NumberFormat('#,##0.00', 'pt_BR');
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final _pizzaBloc = BlocProvider.getBloc<PizzaBloc>();
  final _carrinhoBloc = BlocProvider.getBloc<CarrinhoBloc>();

  @override
  Widget build(BuildContext context) {
    final dim = Dimensoes(context: context);

    Widget configuraCabecalho(BuildContext context) {
      return SliverAppBar(
        elevation: 2,
        expandedHeight: dim.expandedHeight,
        floating: false,
        pinned: true,
        snap: false,
        backgroundColor: Colors.white,
        flexibleSpace: Container(
          color: Colors.grey[300],
          child: Stack(
            fit: StackFit.expand,
            children: [
              Image.asset('imagens/caixa.png', fit: BoxFit.cover),
              FlexibleSpaceBar(
                centerTitle: true,
                title: Text(
                  'Carrinho',
                  style: TextStyle(color: Colors.white, fontSize: 24, shadows: [
                    Shadow(
                        blurRadius: 0.4,
                        offset: Offset(0.5, 0.5),
                        color: Colors.black)
                  ]),
                ),
              )
            ],
          ),
        ),
      );
    }

    Widget montaCampo(BuildContext context, String etiqueta, Widget widget) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(width: 8),
          Text(
            etiqueta,
            style: TextStyle(
                decoration: TextDecoration.none,
                letterSpacing: 0.2,
                fontWeight: FontWeight.w300,
                fontSize: 14,
                color: Colors.black),
            textAlign: TextAlign.center,
          ),
          SizedBox(width: 4),
          widget
        ],
      );
    }

    Widget textoDestacado(String texto) {
      return Text(
        texto,
        textAlign: TextAlign.start,
        overflow: TextOverflow.ellipsis,
        maxLines: 1,
        style: TextStyle(
            decoration: TextDecoration.none,
            fontWeight: FontWeight.w400,
            letterSpacing: 0.2,
            fontSize: 16,
            color: Colors.black),
      );
    }

    Widget editorDeQuantidade(ItemCarrinho itemCarrinho) {
      return Row(
        children: [
          SizedBox(width: 4),
          FancyButton(Icons.add,
              onTap: () => _carrinhoBloc.aumentaQtd(itemCarrinho.idItem),
              color: Colors.grey[200]),
          SizedBox(width: 8),
          textoDestacado('${itemCarrinho.quantidade}'),
          SizedBox(width: 8),
          FancyButton(itemCarrinho.quantidade > 1 ? Icons.remove : Icons.delete,
              onTap: () {
            itemCarrinho.quantidade > 1
                ? _carrinhoBloc.reduzQtd(itemCarrinho.idItem)
                : _carrinhoBloc.removeItem(itemCarrinho);
          }, color: Colors.grey[200])
        ],
      );
    }

    Widget montaLinha(ItemCarrinho itemCarrinho) {
      final decoracao = BoxDecoration(
          color: Colors.white,
          border: Border(
              left: BorderSide(width: 1, color: Colors.grey[400]),
              top: BorderSide(width: 1, color: Colors.grey[400]),
              right: BorderSide(width: 1, color: Colors.grey[400]),
              bottom: BorderSide(width: 1, color: Colors.grey[400])));

      String imagemPequena =
          _pizzaBloc.getItem(itemCarrinho.idPizza).imagemPequena;

      final descricaoDoItem = Column(
        children: [
          montaCampo(context, 'Nome',
              Expanded(child: textoDestacado(itemCarrinho.nome))),
          SizedBox(height: 8),
          montaCampo(
              context,
              'Preço',
              Expanded(
                  child: textoDestacado(
                      'R\$ ${_fmt.format(itemCarrinho.preco)}'))),
          SizedBox(height: 8),
          montaCampo(
              context,
              'Quant.',
              Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [editorDeQuantidade(itemCarrinho)]))
        ],
      );

      if (dim.isPequeno) {
        return Column(children: [
          Container(
              padding: EdgeInsets.fromLTRB(0, 0, 8, 0),
              decoration: decoracao,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Image.network(
                    _pizzaBloc.getImageUrl('$imagemPequena'),
                    width: 82,
                    height: 82,
                    fit: BoxFit.cover,
                  ),
                  Expanded(child: descricaoDoItem)
                ],
              )),
          Divider(height: 8, color: Colors.grey[300])
        ]);
      } else {
        return Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              padding: EdgeInsets.fromLTRB(4, 8, 4, 8),
              decoration: decoracao,
              child: Column(
                children: [
                  Image.network(
                    _pizzaBloc.getImageUrl('$imagemPequena'),
                    width: dim.lado,
                    fit: BoxFit.fitWidth,
                  ),
                  descricaoDoItem
                ],
              ),
            )
          ],
        );
      }
    }

    Widget configuraPagina(BuildContext context) {
      SliverChildDelegate delegate(AsyncSnapshot<dynamic> snapshot) {
        return SliverChildBuilderDelegate((context, indice) {
          var item = snapshot.data[indice];
          return montaLinha(item);
        }, childCount: (snapshot.data as List).length);
      }

      return Container(
        padding: dim.isPortrait
            ? EdgeInsets.fromLTRB(8, 8, 8, 45)
            : EdgeInsets.fromLTRB(32, 8, 32, 45),
        child: CustomScrollView(
          slivers: [
            StreamBuilder(
              initialData: UnmodifiableListView<ItemCarrinho>([]),
              stream: _carrinhoBloc.minhasPizzas,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  if (dim.isPequeno) {
                    return SliverList(delegate: delegate(snapshot));
                  } else {
                    return SliverGrid(
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: dim.qtdGrades,
                          mainAxisSpacing: 4,
                          crossAxisSpacing: 4,
                          childAspectRatio: dim.aspectRatio),
                      delegate: delegate(snapshot),
                    );
                  }
                } else {
                  return CircularProgressIndicator();
                }
              },
            )
          ],
        ),
      );
    }

    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.grey[300],
      body: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          NestedScrollView(
              headerSliverBuilder: (context, _) =>
                  [configuraCabecalho(context)],
              body: RefreshIndicator(
                displacement: 20,
                onRefresh: () async {
                  _carrinhoBloc.carrega();
                },
                child: configuraPagina(context),
              )),
          Container(
            height: dim.isPortrait ? 55 : 42,
            color: Colors.grey[500],
            child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: [
                  StreamBuilder(
                    stream: _carrinhoBloc.totalCarrinho,
                    builder: (context, snapshot) {
                      return Text(
                          'Total R\$ ${_fmt.format(snapshot.hasData ? snapshot.data : 0)}');
                    },
                  )
                ]),
          )
        ],
      ),
    );
  }
}
