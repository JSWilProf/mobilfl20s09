import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mama_mia/bloc/auth_bloc.dart';
import 'package:mama_mia/bloc/carrinho_bloc.dart';
import 'package:mama_mia/bloc/pizza_bloc.dart';
import 'package:mama_mia/dados/item_carrinho.dart';
import 'package:mama_mia/dados/pizza.dart';
import 'package:mama_mia/telas/carrinho.dart';
import 'package:mama_mia/telas/login.dart';
import 'package:mama_mia/widget/funcoes.dart';
import 'package:mama_mia/widget/sliding_painel.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

class PizzaDetalhe extends StatefulWidget {
  final Pizza pizza;

  PizzaDetalhe({Key key, @required this.pizza}) : super(key: key);

  @override
  _PizzaDetalheState createState() => _PizzaDetalheState();
}

class _PizzaDetalheState extends State<PizzaDetalhe> {
  final fmt = NumberFormat('#,##0.00', 'pt_BR');
  final scaffoldKey = GlobalKey<ScaffoldState>();
  final controleDoPainel = PanelController();
  final pizzaBloc = BlocProvider.getBloc<PizzaBloc>();
  final _authBloc = BlocProvider.getBloc<AuthBloc>();
  final _carrinhoBloc = BlocProvider.getBloc<CarrinhoBloc>();

  @override
  Widget build(BuildContext context) {
    final orientacao = MediaQuery.of(context).orientation;

    Widget configuraOCabecalho(BuildContext context) {
      return SliverAppBar(
        elevation: 2,
        expandedHeight: orientacao == Orientation.portrait ? 200 : 100,
        floating: true,
        pinned: true,
        snap: true,
        backgroundColor: Colors.black,
        flexibleSpace: Container(
          child: Stack(
            fit: StackFit.expand,
            children: <Widget>[
              Image.network(
                pizzaBloc.getImageUrl(widget.pizza.imagemGrande),
                fit: BoxFit.fitWidth,
              ),
              FlexibleSpaceBar(
                centerTitle: true,
                title: Text(
                  '${widget.pizza.nome}',
                  style: TextStyle(color: Colors.white, fontSize: 24, shadows: [
                    Shadow(
                        blurRadius: 0.4,
                        offset: Offset(0.5, 0.5),
                        color: Colors.black)
                  ]),
                ),
              )
            ],
          ),
        ),
        actions: [
          StreamBuilder<bool>(
              initialData: false,
              stream: _carrinhoBloc.temPizza,
              builder: (context, snapshot) {
                return snapshot.data
                    ? FlatButton(
                        color: Color.fromARGB(180, 33, 33, 33),
                        shape: CircleBorder(),
                        child: Icon(Icons.shopping_basket, color: Colors.white),
                        onPressed: () async {
                          await Navigator.push(context,
                              MaterialPageRoute(builder: (_) => Carrinho()));
                        },
                      )
                    : Container();
              })
        ],
      );
    }

    Widget montaCampo(String etiqueta, String texto) {
      return Padding(
          padding: EdgeInsets.all(8),
          child: Column(
            children: <Widget>[
              SizedBox(height: 8),
              Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Expanded(
                      child: Text(
                        etiqueta,
                        textAlign: TextAlign.start,
                        style: TextStyle(
                            decoration: TextDecoration.none,
                            letterSpacing: 0.5,
                            fontWeight: FontWeight.w300,
                            fontSize: 18,
                            color: Colors.white70),
                      ),
                    ),
                  ]),
              SizedBox(height: 8),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    child: Text(
                      texto,
                      textAlign: TextAlign.start,
                      overflow: TextOverflow.visible,
                      maxLines: 10,
                      style: TextStyle(
                          decoration: TextDecoration.none,
                          fontWeight: FontWeight.w400,
                          letterSpacing: 0.5,
                          fontSize: 24,
                          color: Colors.white),
                    ),
                  )
                ],
              ),
              SizedBox(height: 8)
            ],
          ));
    }

    Widget configuraAPagina(BuildContext context) {
      return SingleChildScrollView(
        child: Container(
          color: Colors.black,
          padding: EdgeInsets.all(8),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              montaCampo('Grupo', '${widget.pizza.grupo}'),
              montaCampo('Ingredientes', '${widget.pizza.descricao}'),
              montaCampo('Preço', 'R\$ ${fmt.format(widget.pizza.preco)}'),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  FlatButton(
                    padding: EdgeInsets.fromLTRB(16, 12, 16, 12),
                    color: Color.fromARGB(255, 0, 188, 33),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8)),
                    child: Text(
                      'Eu Quero!',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 24,
                          fontWeight: FontWeight.w500,
                          shadows: [
                            Shadow(
                                blurRadius: 0.4,
                                offset: Offset(0.8, 0.8),
                                color: Colors.black)
                          ]),
                    ),
                    onPressed: () async {
                      if (!await _authBloc.estaLogado.first) {
                        controleDoPainel.animatePanelToPosition(1);
                      } else {
                        _carrinhoBloc.adicionaItem(ItemCarrinho()
                          ..idPizza = widget.pizza.id
                          ..nome = widget.pizza.nome
                          ..descricao = widget.pizza.descricao
                          ..preco = widget.pizza.preco
                          ..quantidade = 1);
                        alerta(
                            context: context,
                            scaffoldKey: scaffoldKey,
                            mensagem: 'A Pizza foi adicionada no Carrinho');
                      }
                    },
                  )
                ],
              )
            ],
          ),
        ),
      );
    }

    return Scaffold(
        backgroundColor: Colors.black,
        key: scaffoldKey,
        body: configuraOSlidingPainel(
            context,
            controleDoPainel,
            NestedScrollView(
              headerSliverBuilder: (context, _) =>
                  [configuraOCabecalho(context)],
              body: SafeArea(
                top: orientacao == Orientation.portrait ? false : true,
                bottom: false,
                child: configuraAPagina(context),
              ),
            ),
            Login(scaffoldKey: scaffoldKey, controller: controleDoPainel)));
  }
}
