import 'dart:collection';

import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:intl/intl.dart';
import 'package:mama_mia/bloc/carrinho_bloc.dart';
import 'package:mama_mia/bloc/favorito_bloc.dart';
import 'package:mama_mia/bloc/pizza_bloc.dart';
import 'package:mama_mia/dados/pizza.dart';
import 'package:mama_mia/telas/carrinho.dart';
import 'package:mama_mia/telas/login.dart';
import 'package:mama_mia/telas/pizza_detalhe.dart';
import 'package:mama_mia/widget/drawer.dart';
import 'package:mama_mia/widget/sliding_painel.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

class ListaDePizzas extends StatefulWidget {
  @override
  _ListaDePizzasState createState() => _ListaDePizzasState();
}

final fmt = NumberFormat('#,##0.00', 'pt_BR');

class _ListaDePizzasState extends State<ListaDePizzas> {
  final scaffolKey = GlobalKey<ScaffoldState>();
  final controleDoPainel = PanelController();
  final pizzaBloc = BlocProvider.getBloc<PizzaBloc>();
  final favoritoBloc = BlocProvider.getBloc<FavoritoBloc>();
  final _carrinhoBloc = BlocProvider.getBloc<CarrinhoBloc>();

  @override
  Widget build(BuildContext context) {
    final orientacao = MediaQuery.of(context).orientation;

    Widget configuraOCabecalho() {
      return SliverAppBar(
        elevation: 2,
        expandedHeight: 200,
        floating: true,
        snap: orientacao == Orientation.portrait ? true : false,
        pinned: orientacao == Orientation.portrait ? false : true,
        backgroundColor: Colors.black,
        flexibleSpace: Container(
          child: Stack(
            fit: StackFit.expand,
            children: <Widget>[
              Image.asset(
                'imagens/capa.jpg',
                fit: BoxFit.cover,
              ),
              FlexibleSpaceBar(
                centerTitle: true,
                title: Text(
                  'Mama Mia',
                  style: TextStyle(color: Colors.white, fontSize: 24, shadows: [
                    Shadow(
                        blurRadius: 0.4,
                        offset: Offset(0.5, 0.5),
                        color: Colors.black)
                  ]),
                ),
              ),
            ],
          ),
        ),
        actions: [
          StreamBuilder<bool>(
              initialData: false,
              stream: _carrinhoBloc.temPizza,
              builder: (context, snapshot) {
                return snapshot.data
                    ? FlatButton(
                        color: Color.fromARGB(180, 33, 33, 33),
                        shape: CircleBorder(),
                        child: Icon(Icons.shopping_basket, color: Colors.white),
                        onPressed: () async {
                          await Navigator.push(context,
                              MaterialPageRoute(builder: (_) => Carrinho()));
                        },
                      )
                    : Container();
              })
        ],
      );
    }

    Widget _constroiGrade(Pizza pizza) {
      return GridTile(
        key: Key(pizza.id.toString()),
        child: GestureDetector(
          onTap: () async {
            await Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => PizzaDetalhe(pizza: pizza)));
          },
          child: Image.network(
            pizzaBloc.getImageUrl(pizza.imagemPequena),
            fit: BoxFit.cover,
          ),
        ),
        footer: GridTileBar(
          backgroundColor: Colors.black45,
          title: FittedBox(
            fit: BoxFit.scaleDown,
            child: Text('${pizza.nome}'),
            alignment: Alignment.centerLeft,
          ),
          subtitle: FittedBox(
            fit: BoxFit.scaleDown,
            child: Text('${fmt.format(pizza.preco)}'),
            alignment: Alignment.centerLeft,
          ),
          trailing: StreamBuilder<List<int>>(
            stream: favoritoBloc.osFavoritos,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return GestureDetector(
                  child: Icon(
                      snapshot.data.contains(pizza.id)
                          ? Icons.favorite
                          : Icons.favorite_border,
                      color: Colors.white),
                  onTap: () => favoritoBloc.trocaFavorito(pizza),
                );
              } else {
                return CircularProgressIndicator();
              }
            },
          ),
        ),
      );
    }

    Widget configuraAPagina() {
      return StreamBuilder(
        stream: pizzaBloc.minhasPizzas,
        initialData: UnmodifiableListView<Pizza>([]),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return SliverGrid(
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: orientacao == Orientation.portrait ? 2 : 3,
                  childAspectRatio:
                      orientacao == Orientation.portrait ? 1.0 : 1.3,
                  mainAxisSpacing: 4,
                  crossAxisSpacing: 4),
              delegate: SliverChildBuilderDelegate(
                  (context, indice) => _constroiGrade(snapshot.data[indice]),
                  childCount: (snapshot.data as List).length),
            );
          } else {
            return CircularProgressIndicator();
          }
        },
      );
    }

    return Scaffold(
        key: scaffolKey,
        backgroundColor: Colors.black,
        drawer: AppDrawer(controller: controleDoPainel),
        body: configuraOSlidingPainel(
            context,
            controleDoPainel,
            NestedScrollView(
              headerSliverBuilder: (context, _) => [configuraOCabecalho()],
              body: RefreshIndicator(
                displacement: 20,
                onRefresh: () async {
                  pizzaBloc.carrega();
                },
                child: CustomScrollView(
                  slivers: <Widget>[configuraAPagina()],
                ),
              ),
            ),
            Login(scaffoldKey: scaffolKey, controller: controleDoPainel)));
  }
}
