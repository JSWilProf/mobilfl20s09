import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:mama_mia/bloc/auth_bloc.dart';
import 'package:mama_mia/telas/cadastro.dart';
import 'package:mama_mia/widget/funcoes.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

class Login extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;
  final PanelController controller;

  Login({Key key, @required this.scaffoldKey, @required this.controller})
      : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final _emailController = TextEditingController();
  final _senhaController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  final _authBloc = BlocProvider.getBloc<AuthBloc>();

  @override
  Widget build(BuildContext context) {
    final progressDialog = ProgressDialog(context)
      ..style(message: 'Efetuando o Login...');

    return Scaffold(
      body: Form(
        key: _formKey,
        child: ListView(
          physics: ClampingScrollPhysics(),
          padding: EdgeInsets.only(left: 16, right: 16),
          children: [
            SizedBox(height: 12),
            Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              Container(
                width: 30,
                height: 5,
                decoration: BoxDecoration(
                    color: Colors.grey[300],
                    borderRadius: BorderRadius.all(Radius.circular(12))),
              )
            ]),
            SizedBox(height: 18),
            TextFormField(
              controller: _emailController,
              decoration: InputDecoration(hintText: 'E-Mail'),
              keyboardType: TextInputType.emailAddress,
              validator: (value) => value.isEmpty || !value.contains('@')
                  ? 'E-Mail inválido!'
                  : null,
            ),
            SizedBox(height: 16),
            TextFormField(
              controller: _senhaController,
              decoration: InputDecoration(hintText: 'Senha'),
              obscureText: true,
              validator: (value) =>
                  value.isEmpty || value.length < 6 ? 'Senha inválida!' : null,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                FlatButton(
                  child: Text('Criar conta', textAlign: TextAlign.left),
                  padding: EdgeInsets.only(right: 10),
                  onPressed: () {
                    widget.controller.close();
                    Navigator.push(widget.scaffoldKey.currentContext,
                        MaterialPageRoute(builder: (_) => Cadastro()));
                  },
                ),
                FlatButton(
                  child:
                      Text('Esqueci minha senha', textAlign: TextAlign.right),
                  padding: EdgeInsets.zero,
                  onPressed: () {
                    if (_emailController.text.isEmpty) {
                      alerta(
                          context: context,
                          scaffoldKey: widget.scaffoldKey,
                          mensagem: 'Insira seu e-mail para a recuperação!');
                    } else {
                      _authBloc.recuperarSenha(_emailController.text);
                      alerta(
                          context: context,
                          scaffoldKey: widget.scaffoldKey,
                          mensagem: 'Confira seu e-mail!');
                      widget.controller.close();
                    }
                  },
                ),
              ],
            ),
            SizedBox(height: 16),
            SizedBox(
              height: 44,
              child: RaisedButton(
                child: Text('Entrar', style: TextStyle(fontSize: 18)),
                textColor: Colors.white,
                color: Theme.of(context).primaryColor,
                onPressed: () {
                  _formKey.currentState.validate();

                  progressDialog.show();

                  _authBloc.login(
                      email: _emailController.text,
                      senha: _senhaController.text,
                      noOk: () => progressDialog.hide().then((value) {
                            _emailController.clear();
                            _senhaController.clear();
                            widget.controller.close();
                            FocusManager.instance.primaryFocus.unfocus();
                          }),
                      noErro: () => progressDialog.hide().then((value) =>
                          alerta(
                              context: context,
                              scaffoldKey: widget.scaffoldKey,
                              mensagem: 'Houve falha no Login!',
                              erro: true)));
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
