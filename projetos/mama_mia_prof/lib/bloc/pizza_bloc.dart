import 'dart:collection';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:mama_mia/dados/pizza.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:rxdart/rxdart.dart';

class PizzaBloc extends BlocBase {
  final _pizzaController = BehaviorSubject<UnmodifiableListView<Pizza>>();
  Stream get minhasPizzas => _pizzaController.stream;
  //TODO: ajustar o Endereço do servidor onde estao as Pizzas
  final _pizzaria = 'http://192.168.0.254:8080/pizza';
  List<Pizza> _asPizzas;

  Pizza getItem(int id) => _asPizzas.firstWhere((item) => item.id == id);

  PizzaBloc() {
    carrega();
  }

  void carrega() async {
    _asPizzas = await _pizzas();
    _pizzaController.sink.add(UnmodifiableListView(_asPizzas));
  }

  Future<List<Pizza>> _pizzas() async {
    http.Response response = await http.get('$_pizzaria/api/lista');
    if (response.statusCode == 200) {
      var dados = json.decode(response.body) as List;
      return dados.map((item) => Pizza.fromJson(item)).toList();
    } else {
      throw Exception('Houve falha ao carregar a lista de Pizzas');
    }
  }

  String getImageUrl(String nome) {
    return '$_pizzaria/images/$nome';
  }

  @override
  void dispose() {
    _pizzaController.close();
    super.dispose();
  }
}
