import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:mama_mia/dados/pizza.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FavoritoBloc extends BlocBase {
  final _favoritoController = BehaviorSubject<List<int>>();
  Stream<List<int>> get osFavoritos => _favoritoController.stream;
  List<int> _favoritos = [];

  FavoritoBloc() {
    SharedPreferences.getInstance().then((prefs) {
      if(prefs.getKeys().contains("favoritos")) {
        var asPreferencias = prefs.getStringList("favoritos");
        _favoritos = asPreferencias.map((preferencia) =>
            int.parse(preferencia)).toList();
      }
      _favoritoController.add(_favoritos);
    });
  }

  void trocaFavorito(Pizza pizza) {
    if(_favoritos.contains(pizza.id)) {
      _favoritos.remove(pizza.id);
    } else {
      _favoritos.add(pizza.id);
    }

    _favoritoController.add(_favoritos);

    SharedPreferences.getInstance().then((prefs) {
      prefs.setStringList("favoritos",
        _favoritos.map((preferencia) => '$preferencia').toList());
    });
  }

  @override
  void dispose() {
    _favoritoController.close();
    super.dispose();
  }
}