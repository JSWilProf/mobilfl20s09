import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:mama_mia/dados/cliente.dart';
import 'package:rxdart/rxdart.dart';

class AuthBloc extends BlocBase {
  final _cliente = BehaviorSubject<Cliente>();
  final _estaLogado = BehaviorSubject<bool>();
  Stream<bool> get estaLogado => _estaLogado.stream;

  final _firestore = FirebaseFirestore.instance;
  final _auth = FirebaseAuth.instance;
  User _firebaseUser;
  Stream<User> get state => _auth.authStateChanges();

  AuthBloc() {
    _init();
  }

  Future<Cliente> get cliente async => _cliente.first;

  Future<Null> _salvar(Cliente cliente) async {
    _cliente.add(cliente);

    await _firestore
        .collection('usuarios')
        .doc(_firebaseUser.uid)
        .set(cliente.toMap());
  }

  void _init() async {
    state.listen((user) async {
      _estaLogado.add(user != null);
      _firebaseUser = user;

      if (_firebaseUser != null) {
        DocumentSnapshot docCliente = await _firestore
            .collection('usuarios')
            .doc(_firebaseUser.uid)
            .get();

        Cliente cliente = Cliente.fromDocument(docCliente);
        _cliente.sink.add(cliente);
      } else {
        _firebaseUser = null;
        _cliente.add(null);
      }
    });
  }

  void inscrever(
      {@required Cliente cliente,
      @required String senha,
      @required VoidCallback noOk,
      @required VoidCallback noErro}) {
    _auth
        .createUserWithEmailAndPassword(email: cliente.email, password: senha)
        .then((auth) async {
      _firebaseUser = auth.user;
      cliente.idCliente = _firebaseUser.uid;

      await _salvar(cliente);

      noOk();
    }).catchError((erro) {
      noErro();
    });
  }

  void login(
      {@required String email,
      @required String senha,
      @required VoidCallback noOk,
      @required VoidCallback noErro}) {
    _auth
        .signInWithEmailAndPassword(email: email, password: senha)
        .then((auth) {
      noOk();
    }).catchError((erro) {
      noErro();
    });
  }

  void recuperarSenha(String email) {
    _auth.sendPasswordResetEmail(email: email);
  }

  void logoff() async {
    await _auth.signOut();
  }

  @override
  void dispose() {
    _cliente.close();
    _estaLogado.close();
    super.dispose();
  }
}
