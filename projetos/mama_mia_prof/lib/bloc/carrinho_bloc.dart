import 'dart:collection';

import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mama_mia/bloc/auth_bloc.dart';
import 'package:mama_mia/dados/item_carrinho.dart';
import 'package:rxdart/subjects.dart';

class CarrinhoBloc extends BlocBase {
  final _carrinhoController =
      BehaviorSubject<UnmodifiableListView<ItemCarrinho>>();
  final _carrinhoTemItens = BehaviorSubject<bool>();
  final _carrinhoValorTotal = BehaviorSubject<double>();

  Stream get minhasPizzas => _carrinhoController.stream;
  Stream get temPizza => _carrinhoTemItens.stream;
  Stream get totalCarrinho => _carrinhoValorTotal.stream;

  final _authBloc = BlocProvider.getBloc<AuthBloc>();
  final _firestore = FirebaseFirestore.instance;

  CollectionReference _carrinhoFirebase(idUsuario) =>
      _firestore.collection('usuarios').doc(idUsuario).collection('carrinho');

  var _itens = List<ItemCarrinho>();

  CarrinhoBloc() {
    carrega();
  }

  double get _calculaTotalCarrinho => _itens.length > 0
      ? _itens
          .map((item) => item.total)
          .reduce((total, valor) => total += valor)
      : .0;

  void _atualiza() {
    _carrinhoController.sink.add(UnmodifiableListView(_itens));
    _carrinhoTemItens.sink.add(_itens != null && _itens.length > 0);
    _carrinhoValorTotal.sink.add(_calculaTotalCarrinho);
  }

  void carrega() async {
    _authBloc.state.listen((user) async {
      if (user != null) {
        _carrinhoFirebase(user?.uid).get().then((lista) {
          _itens = lista.docs
              .map((item) => ItemCarrinho.fromDocument(item))
              .toList();
          _atualiza();
        });
      } else {
        _itens = List<ItemCarrinho>();
        _atualiza();
      }
    });
  }

  void adicionaItem(ItemCarrinho item) async {
    var itemEncontrado = _itens.length > 0
        ? _itens.firstWhere((oItem) => oItem.nome == item.nome,
            orElse: () => null)
        : null;

    if (itemEncontrado != null) {
      aumentaQtd(itemEncontrado.idItem);
    } else {
      await _authBloc.cliente.then((cliente) {
        if (cliente != null) {
          _itens.add(item);
          _carrinhoFirebase(cliente.idCliente).add(item.toMap()).then((doc) {
            item.idItem = doc.id;
            _carrinhoFirebase(cliente.idCliente)
                .doc(item.idItem)
                .set(item.toMap());
          });

          _atualiza();
        }
      });
    }
  }

  ItemCarrinho getItem(String id) =>
      _itens.firstWhere((item) => item.idItem == id);

  void _updateData(ItemCarrinho item) async {
    var cliente = await _authBloc.cliente;
    _carrinhoFirebase(cliente.idCliente).doc(item.idItem).update(item.toMap());
    _atualiza();
  }

  void atualizaItem(ItemCarrinho item) async {
    var indice = _itens.indexWhere((oItem) => oItem.idItem == item.idItem);
    _itens[indice] = item;
    _updateData(item);
  }

  void removeItem(ItemCarrinho item) async {
    var cliente = await _authBloc.cliente;
    _carrinhoFirebase(cliente.idCliente).doc(item.idItem).delete();
    _itens.remove(item);

    _atualiza();
  }

  void reduzQtd(String id) {
    var item = getItem(id);
    if (item.quantidade > 0) {
      item.quantidade--;
      _updateData(item);
    }
  }

  void aumentaQtd(String id) {
    var item = getItem(id);
    if (item.quantidade < 5) {
      item.quantidade++;
      _updateData(item);
    }
  }

  @override
  void dispose() {
    _carrinhoController.close();
    _carrinhoTemItens.close();
    _carrinhoValorTotal.close();
    super.dispose();
  }
}
