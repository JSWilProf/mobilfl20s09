import 'dart:convert';
import 'dart:typed_data';

import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:mama_mia/bloc/auth_bloc.dart';
import 'package:rxdart/subjects.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FotoBloc extends BlocBase {
  final _localFoto = BehaviorSubject<Uint8List>();
  Stream<Uint8List> get localFoto => _localFoto.stream;
  final _authBloc = BlocProvider.getBloc<AuthBloc>();
  String _email = '';

  FotoBloc() {
    _init();
  }

  _init() {
    _authBloc.state.listen((user) async {
      _email = user?.email;

      if (_email != null) {
        var prefs = await SharedPreferences.getInstance();
        if (prefs.containsKey(_email)) {
          _localFoto.add(Base64Decoder().convert(prefs.getString(_email)));
        }
      } else {
        _localFoto.add(null);
      }
    });
  }

  Future<void> setLocalFoto(Uint8List foto) async {
    _localFoto.add(foto);
    var prefs = await SharedPreferences.getInstance();
    if (_email != null) {
      prefs.setString(_email, Base64Encoder().convert(foto));
    }
  }

  Future<void> deleteLocalFoto() async {
    _localFoto.add(null);
    var prefs = await SharedPreferences.getInstance();
    if (_email != null && prefs.containsKey(_email)) {
      prefs.remove(_email);
    }
  }

  @override
  void dispose() {
    _localFoto.close();
    super.dispose();
  }
}
