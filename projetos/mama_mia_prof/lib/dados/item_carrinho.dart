import 'package:cloud_firestore/cloud_firestore.dart';

class ItemCarrinho {
  String idItem;
  int idPizza;
  String nome;
  String descricao;
  double preco;
  int quantidade;

  double get total => quantidade * preco;

  ItemCarrinho();

  ItemCarrinho.fromDocument(DocumentSnapshot snapshot) {
    idItem = snapshot.id;
    idPizza = snapshot.data()['idPizza'];
    nome = snapshot.data()['nome'];
    descricao = snapshot.data()['descricao'];
    preco = snapshot.data()['preco'];
    quantidade = snapshot.data()['quantidade'];
  }

  Map<String, dynamic> toMap() => {
        'idItem': idItem,
        'idPizza': idPizza,
        'nome': nome,
        'descricao': descricao,
        'preco': preco,
        'quantidade': quantidade
      };
}
