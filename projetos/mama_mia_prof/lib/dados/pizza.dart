class Pizza {
  int id;
  String nome;
  String grupo;
  String descricao;
  String imagemGrande;
  String imagemPequena;
  double preco;

  Pizza(
      {this.id,
      this.nome,
      this.grupo,
      this.descricao,
      this.imagemGrande,
      this.imagemPequena,
      this.preco});

  factory Pizza.fromJson(dynamic json) {
    return Pizza(
        id: json["id"],
        nome: json["nome"],
        descricao: json["descricao"],
        grupo: json["grupo"],
        imagemGrande: json["imagemGrande"],
        imagemPequena: json["imagemPequena"],
        preco: json["preco"]);
  }

  @override
  String toString() {
    return 'Id: $id Nome: $nome';
  }
}
