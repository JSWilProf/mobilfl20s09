import 'package:cloud_firestore/cloud_firestore.dart';

class Cliente {
  String idCliente;
  String nome;
  String email;

  Cliente();

  Cliente.fromDocument(DocumentSnapshot snapshot) {
    idCliente = snapshot.id;
    nome = snapshot.data()['nome'];
    email = snapshot.data()['email'];
  }

  Map<String, dynamic> toMap() {
    return {'idCliente': idCliente, 'nome': nome, 'email': email};
  }

  @override
  String toString() {
    return 'idCliente: $idCliente Nome: $nome E-Mail: $email';
  }
}
