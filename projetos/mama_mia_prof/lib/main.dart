import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:mama_mia/bloc/auth_bloc.dart';
import 'package:mama_mia/bloc/carrinho_bloc.dart';
import 'package:mama_mia/bloc/favorito_bloc.dart';
import 'package:mama_mia/bloc/foto_bloc.dart';
import 'package:mama_mia/bloc/pizza_bloc.dart';
import 'package:mama_mia/telas/lista_de_pizzas.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  runApp(BlocProvider(
    blocs: [
      Bloc((_) => PizzaBloc()),
      Bloc((_) => FavoritoBloc()),
      Bloc((_) => AuthBloc()),
      Bloc((_) => CarrinhoBloc()),
      Bloc((_) => FotoBloc())
    ],
    child: MaterialApp(
      home: ListaDePizzas(),
    ),
  ));
}
