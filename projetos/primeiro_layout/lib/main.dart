import 'package:flutter/material.dart';

void main() => runApp(ReceitaApp());

class ReceitaApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(primarySwatch: Colors.blue),
      title: 'Flutter Demo',
      home: ReceitaPage(title: 'Flutter Demo Home Page'),
    );
  }
}

class ReceitaPage extends StatefulWidget {
  ReceitaPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _ReceitaPageState createState() => _ReceitaPageState();
}

class _ReceitaPageState extends State<ReceitaPage> {
  @override
  Widget build(BuildContext context) {
    var descricao = Container(
      padding: EdgeInsets.fromLTRB(20, 10, 20, 20),
      child: Text(
        '    Pavlova é uma sobremesa à base '
        'de merengue com o nome de bailarina russa '
        'Anna Pavlova.\n    Pavlova apresenta uma '
        'crosta crocante e macio, leve por '
        'dentro, coberto com frutas e chantilly.',
        textAlign: TextAlign.justify,
        overflow: TextOverflow.visible,
        maxLines: 10,
        style: TextStyle(
            fontFamily: 'Roboto',
            fontWeight: FontWeight.w300,
            letterSpacing: 0.5,
            fontSize: 20),
      ),
    );

    var titulo = Container(
      padding: EdgeInsets.fromLTRB(20, 20, 20, 10),
      child: Text('Pavlora de Morango',
          style: TextStyle(
              fontFamily: 'Roboto',
              fontWeight: FontWeight.w900,
              letterSpacing: 0.5,
              fontSize: 30)),
    );

    var estilo = TextStyle(
        fontFamily: 'Roboto',
        fontWeight: FontWeight.w800,
        letterSpacing: 0.5,
        color: Colors.black,
        fontSize: 18);

    var icones = DefaultTextStyle.merge(
        style: estilo,
        child: Container(
          padding: EdgeInsets.all(20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Column(
                children: <Widget>[
                  Icon(Icons.kitchen, color: Colors.green[500]),
                  Text('Preparaçao'),
                  Text('25 min')
                ],
              ),
              Column(
                children: <Widget>[
                  Icon(Icons.timer, color: Colors.green[500]),
                  Text('Cozimento'),
                  Text('1 hora')
                ],
              ),
              Column(
                children: <Widget>[
                  Icon(Icons.restaurant, color: Colors.green[500]),
                  Text('Serve'),
                  Text('4 - 6')
                ],
              ),
            ],
          ),
        ));

    return SafeArea(
      top: true,
      bottom: false,
      child: Scaffold(
        body: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Image.asset('imagens/img0.png', fit: BoxFit.fitWidth),
              titulo,
              descricao,
              icones,
            ],
          ),
        ),
      ),
    );
  }
}
